import { Injectable } from '@angular/core';
import { CarListItem } from "../CarListItem";
import { Car } from "../Car";
import { CarBooking } from "../CarBooking";
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CarRentalService {
    constructor(private http: Http) { }
    private apiBaseUrl: string = "https://car-rental-176710.appspot.com/_ah/api/carRental/v1/"
   
    private headers = new Headers({ 'Content-Type': 'application/json' });


    getAvailableCars(): Promise<CarListItem[]> {
        return this.http.get(this.apiBaseUrl + "cars")
            .toPromise()
            .then(response => response.json().items as CarListItem[])
            .catch(this.handleError);
    }

    getCar(carId:number): Promise<Car> {
        return this.http.get(this.apiBaseUrl + "car/" + carId)
            .toPromise()
            .then(response => response.json() as Car)
            .catch(this.handleError);
    }

    sendBooking (booking:CarBooking) : void {
        this.http.post(this.apiBaseUrl + "booking", booking)
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}