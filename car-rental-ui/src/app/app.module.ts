import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdToolbarModule, MdDatepickerModule, MdNativeDateModule, MdInputModule, MdListModule, MdIconModule, MdButtonModule, MdCardModule, MdDialogModule , MdSelectModule, MdSnackBarModule} from '@angular/material';
import { HttpModule, JsonpModule }    from '@angular/http';
import { AppComponent, BookingDialog } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    BookingDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MdToolbarModule,
    MdNativeDateModule,
    MdDatepickerModule,
    MdInputModule,
    MdListModule,
    MdIconModule,
    MdButtonModule,
    MdCardModule,
    MdDialogModule,
    MdSelectModule,
    MdSnackBarModule,
    HttpModule,
    JsonpModule
  ],
  entryComponents: [
    BookingDialog
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
