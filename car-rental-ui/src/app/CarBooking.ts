export enum Usage {
    DOMESTIC, FOREIGN
}
export class CarBooking {
    id:number;

    carId:number;
	
	customerName: string;

	customerEmail: string;

	customerLincenceNumber: string;

	bookStartDate: Date;

	bookEndDate: Date;

	usage: Usage;

	foreignCountries:string[];

	
}