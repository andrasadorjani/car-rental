export enum Transmission {
    MANUAL,
    AUTOMATIC
}

export enum FuelType {
    DIESEL,
    PETROL
}

export enum BodyType {
    SEDAN,
    HATCH,
    SUV,
    CONVERTIBLE,
    COUPE,
    WAGON
}

export class Car {
    id:number;
	make:string;
	model:string;
	colour:string;
	bodyType:BodyType;
	transmission:Transmission;
    fuleType:FuelType;
    seats:number;
	doors:number;
    buildDate:Date;
}