import { Component, ViewEncapsulation, OnInit, Inject } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MdToolbarModule, MdDatepickerModule, MdDialogModule, MdDialogRef, MdDialog, MD_DIALOG_DATA } from '@angular/material';
import { CarRentalService } from './service/carrental.service';
import { CarListItem } from "./CarListItem";
import { Car } from "./Car";
import { CarBooking, Usage } from "./CarBooking";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CarRentalService]
})
export class AppComponent implements OnInit{
  constructor(
    private carRentalService: CarRentalService, public bookingDialog: MdDialog
  ) {
    this.availableCars = new Array<CarListItem>();
    
  }
  availableCars:CarListItem[];
  selectedCar:Car;
  
  ngOnInit(){
    this.carRentalService.getAvailableCars().then(carListItems => this.carsLoaded(carListItems));
  }
  
  loadCarInfo(carListItem:CarListItem) : void {
    this.carRentalService.getCar(carListItem.id).then(car => this.carDataLoaded(car));
  }

  carsLoaded(cars: CarListItem[]) : void {
    this.availableCars = cars;
    console.log(this.availableCars);
  }

  carDataLoaded (car: Car) :void {
    this.selectedCar = car;
    console.log(this.selectedCar);
  }

  openBookingDialog(carListItem:CarListItem) {
    let dialogRef = this.bookingDialog.open(BookingDialog, {
      data: carListItem,
    });
    this.loadCarInfo(carListItem);
    dialogRef.afterClosed().subscribe(result => {
      if(result instanceof CarBooking) {
        this.sendBooking(result);
      }else{
        console.log("booking cancelled");
      }
    });
  }

  sendBooking(booking:CarBooking) : void {
    this.carRentalService.sendBooking(booking), this.selectedCar.id;
    console.log(booking);
  }
}

@Component({
  selector: 'booking-dialog',
  templateUrl: 'booking-dialog.html',
})
export class BookingDialog {
  booking:CarBooking;
  usages = [
    {value: 'DOMESTIC', viewValue: 'domestic'},
    {value: 'DOMESTIC', viewValue: 'foreign'}
  ];
  constructor(@Inject(MD_DIALOG_DATA) public carListItem: CarListItem, public dialogRef: MdDialogRef<BookingDialog>) {
    this.booking = new CarBooking();
    this.booking.carId = carListItem.id;
  }
}
