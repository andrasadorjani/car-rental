/**
 * 
 */
package hu.adorjani.msci.carrental.domain;

import java.util.Date;
import java.util.Set;

import org.joda.time.Interval;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ador
 *
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class CarBooking extends AbstractEntityWithParent<Car> {

	private static final long serialVersionUID = 1;
	
	public static interface IndexedProperties extends AbstractEntity.IndexedProperties {
		String BOOK_START_DATE = "bookStartDate";
		String BOOK_END_DATE = "bookEndDate";
	}
	
	public static interface Properties extends AbstractEntity.Properties {
		String CUSTOMER_NAME = "customerName";
		String CUSTOMER_EMAIL = "customerEmail";
		String CUSTOMER_LICENCE_NUMBER = "customerLincenceNumber";
		String USAGE = "usage";
		String FOREIGN_COUNTRIES = "foreignCountries";
	}
	
	public CarBooking(Long carId) {
		super(Key.create(Car.class, carId));
	}
	
	private String customerName;
	
	private String customerEmail;
	
	private String customerLincenceNumber;
	
	@Index
	private Date bookStartDate;
	
	@Index
	private Date bookEndDate;
	
	private Usage usage;
	
	private Set<String> foreignCountries;

	@Ignore
	private Interval bookInterval;
	
	@OnLoad
	protected void setBookInterval () {
		bookInterval = new Interval(bookStartDate.getTime(), bookEndDate.getTime());
	}
	
	@Builder
	protected CarBooking(Long carId, String customerName, String customerEmail, String customerLincenceNumber,
			Date bookStartDate, Date bookEndDate, Usage usage, Set<String> foreignCountries) {
		this(carId);
		this.customerName = customerName;
		this.customerEmail = customerEmail;
		this.customerLincenceNumber = customerLincenceNumber;
		this.bookStartDate = bookStartDate;
		this.bookEndDate = bookEndDate;
		this.usage = usage;
		this.foreignCountries = foreignCountries;
		this.setBookInterval();
	}

	public static enum Usage {
		DOMESTIC,
		FOREIGN
	}
	
}
