package hu.adorjani.msci.carrental.business;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Interval;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.googlecode.objectify.Key;

import hu.adorjani.msci.carrental.domain.Car;
import hu.adorjani.msci.carrental.domain.CarBooking;
import hu.adorjani.msci.carrental.domain.CarBooking.Usage;
import hu.adorjani.msci.carrental.storage.objectify.StorageManager;
import hu.adorjani.msci.carrental.storage.objectify.StorageManagerImpl;

public class CarRentalServiceImpl implements CarRentalService {

	private static final StorageManager<Car> carManager = new StorageManagerImpl<>(Car.class);
	private static final StorageManager<CarBooking> bookingManager = new StorageManagerImpl<>(CarBooking.class);
	
	
	@Override
	public List<Car> listAvailableCars() {
		return carManager.getAll();
	}

	@Override
	public Car getCarById(Long id) {
		Preconditions.checkNotNull(id);
		return carManager.get(id);
	}

	@Override
	public Car saveCar(Car newCar) {
		Preconditions.checkNotNull(newCar);
		carManager.save(newCar);
		return newCar;
	}

	@Override
	public CarBooking bookACar(CarBooking newBooking, Long carId) throws BusinessException {
		Preconditions.checkNotNull(carId);
		checkDateConstraints(newBooking);
		checkCollisionWithValidBookings(newBooking, carId);
		if (newBooking.getUsage().equals(Usage.FOREIGN)) {
			//TODO check availability in foreign countries, calling other service to check.
		}
		bookingManager.save(newBooking);
		
		return newBooking;
	}

	private void checkCollisionWithValidBookings(final CarBooking newBooking, Long carId) throws BusinessException {
		List<CarBooking> validBookings = bookingManager.filter(CarBooking.IndexedProperties.BOOK_END_DATE + " >=", new Date()).ancestor(Key.create(Car.class, carId)).list();
		List<Interval> validBookIntervals = Lists.transform(validBookings, new Function<CarBooking, Interval>() {
			@Override
			public Interval apply(CarBooking carBooking) {
				return carBooking.getBookInterval();
			}
		});
		
		Collection<Interval> overlapedBookingIntervals = Collections2.filter(validBookIntervals, new Predicate<Interval>() {
			@Override
			public boolean apply(Interval anInterval) {
				return anInterval.overlaps(newBooking.getBookInterval());
			}
		});
		if(!overlapedBookingIntervals.isEmpty()) {
			throw new BusinessException("The booking period has conflict with other already booked periods.");
		}
		
	}

	private void checkDateConstraints(CarBooking newBooking) throws BusinessException {
		DateTime startDate = new DateTime(newBooking.getBookStartDate());
		DateTime endDate = new DateTime(newBooking.getBookEndDate());
		
		if(startDate.isBeforeNow()) {
			throw new BusinessException("The start date of a new booking cannot be in the past. (" + startDate.toString() + ")");
		}
		
		if(endDate.isBefore(startDate)) {
			throw new BusinessException("The end date of a booking cannot be before the start date. (start date: " + startDate.toString() + ", end date: " + startDate.toString() + ")");
		}
		
	}

}
