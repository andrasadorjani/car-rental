package hu.adorjani.msci.carrental.api;

import java.util.List;

import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.repackaged.com.google.common.base.Preconditions;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import hu.adorjani.msci.carrental.business.BusinessException;
import hu.adorjani.msci.carrental.business.CarRentalService;
import hu.adorjani.msci.carrental.domain.Car;
import hu.adorjani.msci.carrental.domain.CarBooking;

/**
 * @author ador
 *
 */
public class ApiCarRentalServiceImpl implements ApiCarRentalService {

	@Inject
	private CarRentalService carRentalService;
	
	
	@Override
	public List<ApiCarListItem> getAvailableCars() {
		List<Car> availableCars = carRentalService.listAvailableCars();
		return Lists.transform(availableCars, CAR_TO_CAR_LIST_ITEM);
	}

	@Override
	public ApiCar getCarById(Long id) throws NotFoundException {
		Car car = carRentalService.getCarById(id);
		if(car == null) {
			throw new NotFoundException("The car with id '" + id + "' does not exist.");
		}
		return CAR_TO_API_CAR.apply(car);
	}

	@Override
	public ApiCar saveCar(ApiCar newCar) {
		Car car = API_CAR_TO_CAR.apply(newCar);
		Car savedCar = carRentalService.saveCar(car);
		return CAR_TO_API_CAR.apply(savedCar);
	}

	@Override
	public ApiCarBooking bookACar(ApiCarBooking newBooking) throws NotFoundException, BusinessException {
		Long carId = Preconditions.checkNotNull(newBooking.getCarId(), "The car id cannot be null!");
		Car car = carRentalService.getCarById(carId);
		if(car == null) {
			throw new NotFoundException("The car with the id '" + carId + "' not found, you cannot book a reservation");
		}
		
		CarBooking carBooking = CarBooking.builder()
		.carId(carId)
		.bookEndDate(newBooking.getBookEndDate())
		.bookStartDate(newBooking.getBookStartDate())
		.customerEmail(newBooking.getCustomerEmail())
		.customerLincenceNumber(newBooking.getCustomerLincenceNumber())
		.customerName(newBooking.getCustomerName())
		.foreignCountries(newBooking.getForeignCountries())
		.usage(CarBooking.Usage.valueOf(newBooking.getUsage().name()))
		.build();
		
		return BOOKING_TO_API_BOOKING.apply(carRentalService.bookACar(carBooking, carId));
	}
	
	private static final Function<Car, ApiCar> CAR_TO_API_CAR = new Function<Car, ApiCar>() {
		@Override
		public ApiCar apply(Car car) {
			ApiCar apiCar = new ApiCar();
			apiCar.setId(car.getId());
			apiCar.setBodyType(ApiCar.BodyType.valueOf(car.getBodyType().name()));
			apiCar.setBuildDate(car.getBuildDate());
			apiCar.setColour(car.getColour());
			apiCar.setDoors(car.getDoors());
			apiCar.setFuleType(ApiCar.FuelType.valueOf(car.getFuleType().name()));
			apiCar.setMake(car.getMake());
			apiCar.setModel(car.getModel());
			apiCar.setSeats(car.getSeats());
			apiCar.setTransmission(ApiCar.Transmission.valueOf(car.getTransmission().name()));
			return apiCar;
		}
	};
	
	private static Function<Car, ApiCarListItem> CAR_TO_CAR_LIST_ITEM = new Function<Car, ApiCarListItem>() {
		@Override
		public ApiCarListItem apply(Car car) {
			ApiCarListItem carListItem = new ApiCarListItem();
			carListItem.setId(car.getId());
			carListItem.setCarDescription(car.getShortDescription());
			return carListItem;
		}
	};
	
	private static final Function<ApiCar, Car> API_CAR_TO_CAR = new Function<ApiCar, Car>() {
		@Override
		public Car apply(ApiCar apiCar) {
			Car car = Car.builder()
			.bodyType(Car.BodyType.valueOf(apiCar.getBodyType().name()))
			.buildDate(apiCar.getBuildDate())
			.colour(apiCar.getColour())
			.doors(apiCar.getDoors())
			.fuleType(Car.FuelType.valueOf(apiCar.getFuleType().name()))
			.make(apiCar.getMake())
			.model(apiCar.getModel())
			.seats(apiCar.getSeats())
			.transmission(Car.Transmission.valueOf(apiCar.getTransmission().name()))
			.build();
			return car;
		}
	};
	
	private static final Function<CarBooking, ApiCarBooking> BOOKING_TO_API_BOOKING = new Function<CarBooking, ApiCarBooking>() {
		@Override
		public ApiCarBooking apply(CarBooking carBooking) {
			ApiCarBooking apiCarBooking = new ApiCarBooking();
			apiCarBooking.setId(carBooking.getId());
			apiCarBooking.setBookEndDate(carBooking.getBookEndDate());
			apiCarBooking.setBookStartDate(carBooking.getBookStartDate());
			apiCarBooking.setCustomerEmail(carBooking.getCustomerEmail());
			apiCarBooking.setCustomerLincenceNumber(carBooking.getCustomerLincenceNumber());
			apiCarBooking.setCustomerName(carBooking.getCustomerName());
			apiCarBooking.setForeignCountries(carBooking.getForeignCountries());
			apiCarBooking.setUsage(ApiCarBooking.Usage.valueOf(carBooking.getUsage().name()));
			return apiCarBooking;
		}
	};

}
