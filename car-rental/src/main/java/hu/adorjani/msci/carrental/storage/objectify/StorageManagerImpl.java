
package hu.adorjani.msci.carrental.storage.objectify;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.VoidWork;
import com.googlecode.objectify.Work;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Query;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StorageManagerImpl<E> implements StorageManager<E>, Serializable {
	
	public static final double SHORT_DEAD_LINE = 30; // in seconds
	
	private final Class<E> c;
	
	public StorageManagerImpl( Class<E> c ) {
		this.c = c;
		
		synchronized (StorageManagerImpl.class) {
			ObjectifyService.register( c );
		}
	}
	
	private Object readResolve() {
		// this is not really a severe thing but I would like to know if this happen at all
		log.error( "The storage manager was serialized." );
		
		synchronized (StorageManagerImpl.class) {
			ObjectifyService.register( c );
		}
		
		return this;
	}
	
	@Override
	public List<Entity> toEntity( Iterable<E> list ) {
		Objectify ofy = ofy();
		List<Entity> entities = new ArrayList<Entity>();
		for (E e : list) {
			Entity entity = ofy.save().toEntity( e );
			entities.add( entity );
		}
		return entities;
	}
	
	@Override
	public Entity toEntity( E e ) {
		Entity entity = ofy().save().toEntity( e );
		return entity;
	}
	
	@Override
	public List<E> fromEntity( Iterable<Entity> entities ) {
		Objectify ofy = ofy();
		List<E> list = new ArrayList<E>();
		for (Entity entity : entities) {
			E e = ofy.load().fromEntity( entity );
			list.add( e );
		}
		return list;
	}
	
	@Override
	public E fromEntity( Entity entity ) {
		return ofy().load().fromEntity( entity );
	}
	
	@Override
	public Result<Key<E>> saveAsync( E e ) {
		return ofy().save().entity( e );
	}
	
	@Override
	public Result<Map<Key<E>, E>> saveAllAsync( Iterable<E> list ) {
		return ofy().save().<E> entities( list );
	}
	
	@Override
	public E get( Long id ) {
		return getAsync( id ).now();
	}
	
	@Override
	public E get( String id ) {
		return getAsync( id ).now();
	}
	
	@Override
	public Result<E> getAsync( Long id ) {
		return ofy().load().type( c ).id( id );
	}
	
	@Override
	public Result<E> getAsync( String id ) {
		return ofy().load().type( c ).id( id );
	}
	
	@Override
	public Map<Key<E>, E> getAsync( Iterable<Key<E>> keys ) {
		return ofy().load().keys( keys );
	}
	
	@Override
	public Map<String, E> getAsyncByIds( Iterable<String> ids ) {
		if (Iterables.isEmpty( ids )) {
			return Maps.newHashMap();
		}
		return ofy().load().type( c ).ids( ids );
	}
	
	@Override
	public E get( Key<?> parent, String id ) {
		return getAsync( parent, id ).now();
	}
	
	@Override
	public Result<E> getAsync( Key<?> parent, String id ) {
		return ofy().load().key( Key.create( parent, c, id ) );
	}
	
	@Override
	public E get( Key<?> parent, long id ) {
		return getAsync( parent, id ).now();
	}
	
	@Override
	public Result<E> getAsync( Key<?> parent, long id ) {
		return ofy().load().key( Key.create( parent, c, id ) );
	}
	
	@Override
	public Result<Void> deleteAsync( Object e ) {
		return ofy().delete().entity( e );
	}
	
	@Override
	public Result<Void> deleteAllAsync( Iterable<?> e ) {
		return ofy().delete().entities( e );
	}
	
	@Override
	public List<E> getAllChildren( Key<?> parent ) {
		Preconditions.checkNotNull( parent, "the parent can't be null" );
		return Lists.newArrayList( ofy().load().type( c ).ancestor( parent ).chunk( 100 ).iterable() );
	}
	
	@Override
	public List<E> getAll() {
		return Lists.newArrayList( getAllIterable() );
	}
	
	@Override
	public Iterable<E> getAllIterable() {
		return ofy().load().type( c ).chunk( 100 ).iterable();
	}
	
	@Override
	public Iterable<Key<E>> getAllKeyIterable() {
		return ofy().load().type( c ).chunk( 100 ).keys().iterable();
	}
	
	@Override
	public List<Key<E>> getIterableKeyList( String after, int limit ) {
		Query<E> query = ofy().load().type( c ).chunk( Math.min( limit, 100 ) ).limit( limit );
		if (!Strings.isNullOrEmpty( after )) {
			query = query.filterKey( ">", Key.create( c, after ) );
		}
		return query.keys().list();
	}
	
	@Override
	public List<E> getIterableEntityListWithOffsetAndLimit(int offset, int limit) {
	  return ofy().load().type( c ).chunk( Math.min( limit, 100 ) ).offset(offset).limit( limit ).list();
	}
	
	@Override
	public List<E> getIterableEntityList( String property, Object after, int limit ) {
		Query<E> query = ofy().load().type( c ).chunk( Math.min( limit, 100 ) ).limit( limit ).order( property );
		if (after != null) {
			query = query.filter( property + " >=", after );
		}
		return query.list();
	}
	
	@Override
	public int count() {
		return ofy().load().type( c ).chunk( 1000 ).count();
	}
	
	@Override
	public int countChildren( Key<?> key ) {
		return ofy().load().type( c ).ancestor( key ).chunk( 1000 ).count();
	}
	
	@Override
	public LoadType<E> getLoader() {
		return ofy().load().type( c );
	}
	
	@Override
	public List<E> getBy( String byString, Object byObject ) {
		return Lists.newArrayList( getLoader().filter( byString, byObject ).chunk( 100 ).iterable() );
	}
	
	@Override
	public void delete( final Object e ) {
		executeWithRetry( new VoidWork() {
			
			@Override
			public void vrun() {
				deleteAsync( e ).now();
			}
		} );
	}
	
	@Override
	public void deleteAll( final Iterable<?> e ) {
		executeWithRetry( new VoidWork() {
			
			@Override
			public void vrun() {
				deleteAllAsync( e ).now();
			}
		} );
	}
	
	@Override
	public E getSingleBy( String byString, Object byObject ) {
		List<E> list = filter( byString, byObject ).limit( 1 ).list();
		if (list.isEmpty()) {
			return null;
		}
		else {
			return list.get( 0 );
		}
	}
	
	@Override
	public Query<E> filter( String byString, Object byObject ) {
		return ofy().load().type( c ).filter( byString, byObject ).chunk( 100 );
	}
	
	@Override
	public Query<E> order( String byString ) {
		return ofy().load().type( c ).order( byString ).chunk( 100 );
	}
	
	@Override
	public Key<E> save( final E e ) {
		return executeWithRetry( new Work<Key<E>>() {
			
			@Override
			public Key<E> run() {
				return saveAsync( e ).now();
			}
		} );
	}
	
	@Override
	public Map<Key<E>, E> saveAll( final Iterable<E> list ) {
		return executeWithRetry( new Work<Map<Key<E>, E>>() {
			
			@Override
			public Map<Key<E>, E> run() {
				return saveAllAsync( list ).now();
			}
		} );
	}
	
	private <T> T executeWithRetry( Work<T> task ) {
		for (int failures = 0;; failures++) {
			try {
				return task.run();
			}
			catch (ConcurrentModificationException cme) {
				log.info(cme.toString() );
				
				if (failures >= 4) { // this is the fifth failure
					throw cme;
				}
				
				try {
					Thread.sleep( 1000 * (long) (Math.pow( 2, failures ) + Math.random()) );
				}
				catch (InterruptedException ie) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}
