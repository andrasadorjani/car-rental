package hu.adorjani.msci.carrental.api;

import lombok.Data;


@Data
public class ApiCarListItem {
	
	private Long id;
	
	private String carDescription;

}
