/**
 * 
 */
package hu.adorjani.msci.carrental.api;

import java.util.Date;

import lombok.Data;

/**
 * @author ador
 *
 */
@Data
public class ApiCar {
	
	private Long id;
	
	private String make;
	
	private String model;
	
	private String colour;
	
	private BodyType bodyType;
	
	private Transmission transmission;
	
	private FuelType fuleType;
	
	private int seats;
	
	private int doors;
	
	private Date buildDate;
	
	
	public static enum Transmission {
		MANUAL,
		AUTOMATIC
	}
	
	public static enum FuelType {
		DIESEL,
		PETROL
	}
	
	public static enum BodyType {
		SEDAN,
		HATCH,
		SUV,
		CONVERTIBLE,
		COUPE,
		WAGON
	}
}
