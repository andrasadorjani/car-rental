/**
 * 
 */
package hu.adorjani.msci.carrental.business;

import java.util.List;

import hu.adorjani.msci.carrental.domain.Car;
import hu.adorjani.msci.carrental.domain.CarBooking;

/**
 * @author ador
 *
 */
public interface CarRentalService {
	
	List<Car> listAvailableCars();
	
	Car getCarById (Long id);
	
	Car saveCar(Car newCar);
	
	CarBooking bookACar(CarBooking newBooking, Long carId) throws BusinessException;
}
