package hu.adorjani.msci.carrental.storage.objectify;

import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;
import com.google.inject.ImplementedBy;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Query;

@ImplementedBy(StorageManagerImpl.class)
public interface StorageManager<E> {
  
  // get by key
  
  E get(Long id);
  
  Map<String, E> getAsyncByIds(Iterable<String> ids);
  
  Map<Key<E>, E> getAsync(Iterable<Key<E>> keys);
  
  Result<E> getAsync(Long id);
  
  E get(String id);
  
  Result<E> getAsync(String id);
  
  E get(Key<?> parent, String id);
  
  E get(Key<?> parent, long id);
  
  Result<E> getAsync(Key<?> parent, String id);
  
  Result<E> getAsync(Key<?> parent, long id);
  
  List<E> getAll();
  
  Iterable<E> getAllIterable();
  
  /**
   * Returns an iterable for every key in the entity kind.<br/>
   * Careful <b>this may cause timeouts and out of memory errors</b> if you start it on a big table.<br/>
   * Use the {@link #getIterableKeyList(String, int)}
   * 
   * @return key iterable
   */
  Iterable<Key<E>> getAllKeyIterable();
  
  /**
   * Returns a limited number of keys that are greater than the <code>after</code> parameter.<br/>
   * The keys are returned in an ascending order.<br/>
   * You can use this to iterate over huge tables.
   * 
   * @param after you will get the keys that are greater than this
   * @param limit the maximum number of keys returned
   * @return list of key values
   */
  List<Key<E>> getIterableKeyList(String after, int limit);
  
  /**
   * Returns a limited number of entities that are greater than or equal to the <code>after</code> parameter.<br/>
   * The entities are returned in ascending order of the property value.<br/>
   * If the after value is null it will start from the first value.<br/>
   * You can use this to iterate over huge tables.
   * 
   * @param property the property to use for the iteration
   * @param after you will get entities that contain a property value at least this big
   * @param limit the maximum number of entities returned
   * @return list of entities
   */
  List<E> getIterableEntityList(String property, Object after, int limit);
  
  List<E> getAllChildren(Key<?> parent);
  
  // count
  
  int count();
  
  int countChildren(Key<?> key);
  
  // save
  
  Key<E> save(E e);
  
  Result<Key<E>> saveAsync(E e);
  
  Map<Key<E>, E> saveAll(Iterable<E> list);
  
  Result<Map<Key<E>, E>> saveAllAsync(Iterable<E> list);
  
  // entity conversion
  
  Entity toEntity(E e);
  
  List<Entity> toEntity(Iterable<E> list);
  
  E fromEntity(Entity entity);
  
  List<E> fromEntity(Iterable<Entity> list);
  
  // delete
  
  void delete(Object e);
  
  Result<Void> deleteAsync(Object e);
  
  void deleteAll(Iterable<?> e);
  
  Result<Void> deleteAllAsync(Iterable<?> e);
  
  // queries
  
  LoadType<E> getLoader();
  
  E getSingleBy(String byString, Object byObject);
  
  List<E> getBy(String byString, Object byObject);
  
  Query<E> filter(String byString, Object byObject);
  
  Query<E> order(String byString);
  
  List<E> getIterableEntityListWithOffsetAndLimit(int offset, int limit);
  
}
