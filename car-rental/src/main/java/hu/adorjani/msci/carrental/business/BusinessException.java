package hu.adorjani.msci.carrental.business;

import java.io.Serializable;

public class BusinessException extends Exception implements Serializable {
	
	private static final long serialVersionUID = 1;
	
	public BusinessException() {
		super();
	}
	
	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public BusinessException(Throwable cause) {
		super(cause);
	}
}
