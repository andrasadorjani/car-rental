package hu.adorjani.msci.carrental.api;

import java.util.List;

import com.google.api.server.spi.response.NotFoundException;

import hu.adorjani.msci.carrental.business.BusinessException;

public interface ApiCarRentalService {
	
	List<ApiCarListItem> getAvailableCars();
	
	ApiCar getCarById (Long id) throws NotFoundException;
	
	ApiCar saveCar(ApiCar newCar);
	
	ApiCarBooking bookACar(ApiCarBooking newBooking) throws NotFoundException, BusinessException;

}
