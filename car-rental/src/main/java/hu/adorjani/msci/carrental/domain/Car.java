/**
 * 
 */
package hu.adorjani.msci.carrental.domain;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfNotZero;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ador
 *
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Car extends AbstractEntity{
	
	private static final long serialVersionUID = 1;
	
	public static interface IndexedProperties extends AbstractEntity.IndexedProperties {
		String MAKE = "make";
		String BODY_TYPE = "bodyType";
		String TRANSMISSION = "transmission";
		String FUEL_TYPE = "fuleType";
		String SEATS = "seats";
		String DOORS = "doors";
	}
	
	public static interface Properties extends AbstractEntity.Properties {
		String MODEL = "model";
		String COLOUR = "colour";
		String BUILD_DATE = "buildDate";
	}
	
	@Index
	private String make;
	
	private String model;
	
	private String colour;
	
	@Index
	private BodyType bodyType;
	
	@Index
	private Transmission transmission;
	
	@Index
	private FuelType fuleType;
	
	@Index({IfNotNull.class, IfNotZero.class})
	private int seats;
	
	@Index({IfNotNull.class, IfNotZero.class})
	private int doors;
	
	private Date buildDate;
	
	public String getShortDescription () {
		StringBuilder sb = new StringBuilder();
		
		return sb.append(make)
		.append(" ")
		.append(model)
		.append(" ")
		.append(bodyType.name().toLowerCase())
		.append(" ( ")
		.append(seats)
		.append(" seats, with ")
		.append(transmission.name().toLowerCase())
		.append(" transmission )")
		.toString();
	}
	
	@Builder
	protected Car(String make, String model, String colour, BodyType bodyType, Transmission transmission,
			FuelType fuleType, int seats, int doors, Date buildDate) {
		super();
		this.make = make;
		this.model = model;
		this.colour = colour;
		this.bodyType = bodyType;
		this.transmission = transmission;
		this.fuleType = fuleType;
		this.seats = seats;
		this.doors = doors;
		this.buildDate = buildDate;
	}

	public static enum Transmission {
		MANUAL,
		AUTOMATIC
	}
	
	public static enum FuelType {
		DIESEL,
		PETROL
	}
	
	public static enum BodyType {
		SEDAN,
		HATCH,
		SUV,
		CONVERTIBLE,
		COUPE,
		WAGON
	}
}
