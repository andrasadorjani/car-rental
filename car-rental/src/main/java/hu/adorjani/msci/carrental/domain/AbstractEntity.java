package hu.adorjani.msci.carrental.domain;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * @author ador
 *
 */
@Getter
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable{
	
	private static final long serialVersionUID = 1;
	
	public static interface IndexedProperties {
		
	}
	
	public static interface Properties extends IndexedProperties {
		public static final String ID = "id";
	}
	
	@Id
	@NonNull
	private Long id;
	
}
