/**
 * 
 */
package hu.adorjani.msci.carrental.api;

import com.google.api.server.spi.ServiceException;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.ServiceUnavailableException;
import com.google.common.base.MoreObjects;

import hu.adorjani.msci.carrental.business.BusinessException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ApiErrorConverter {

	/**
	 * Converts backend exceptions to Google Cloud Endpoints compatible exceptions.
	 * 
	 * <br>
	 * <code>
	 * Exception															HTTP Status Code			Meaning (from Wikipedia)
	 * com.google.api.server.spi.response.BadRequestException				HTTP 400					"The request cannot be fulfilled due to bad syntax."
	 * com.google.api.server.spi.response.UnauthorizedException			HTTP 401					"Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided."
	 * com.google.api.server.spi.response.ForbiddenException				HTTP 403					"The request was a valid request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference."
	 * com.google.api.server.spi.response.NotFoundException				HTTP 404					"The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible."
	 * com.google.api.server.spi.response.ConflictException				HTTP 409					"Indicates that the request could not be processed because of conflict in the request, such as an edit conflict in the case of multiple updates."
	 * com.google.api.server.spi.response.InternalServerErrorException	HTTP 500					"A generic error message, given when an unexpected condition was encountered and no more specific message is suitable."
	 * com.google.api.server.spi.response.ServiceUnavailableException		HTTP 503					"The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state."
	 * </code>
	 * 
	 * @param t
	 *            the custom Exception
	 * @return Google compatible Exception that can be converted to HTTP error codes
	 */
	public static ServiceException convert(Throwable t) {
		ServiceException convertedException = convertInner(t);
		if (convertedException != null) {
			return convertedException;
		}
		return convertToInternalServerError(t);
	}

	private static ServiceException convertInner(Throwable t) {
		try {
			throw t;
		} catch (NotFoundException | com.googlecode.objectify.NotFoundException e) {
			return convertToNotFound(e);
		}  catch (InternalServerErrorException e) {
			return convertToInternalServerError(e);
		} catch (ServiceUnavailableException e) {
			return e;
		} catch (BusinessException e) {
			if (e.getCause() != null) {
				ServiceException innerException = convertInner(e.getCause());
				return MoreObjects.firstNonNull(innerException, convertToBadRequest(e));
			}
			return convertToBadRequest(e);
		} catch (Throwable t2) {
			if (t2.getCause() != null) {
				return convertInner(t2.getCause());
			}
		}

		return null;
	}

	private static NotFoundException convertToNotFound(Throwable t) {
		NotFoundException e = t instanceof NotFoundException ? (NotFoundException) t : new NotFoundException(t.getMessage(), t);
		log.warn(e.getMessage(), e);
		return e;
	}
	
	
	private static InternalServerErrorException convertToInternalServerError(Throwable t) {
		InternalServerErrorException e = t instanceof InternalServerErrorException ? (InternalServerErrorException) t
				: new InternalServerErrorException(t.getMessage(), t);
		log.error(e.getMessage(), e);
		return e;
	}
	
	private static BadRequestException convertToBadRequest(Throwable t) {
		BadRequestException e = t instanceof BadRequestException ? (BadRequestException) t : new BadRequestException(t.getMessage(), t);
		log.info(e.getMessage(), e);
		return e;
	}

}
