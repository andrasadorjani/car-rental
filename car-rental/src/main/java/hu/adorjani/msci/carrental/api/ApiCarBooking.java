/**
 * 
 */
package hu.adorjani.msci.carrental.api;

import java.util.Date;
import java.util.Set;

import lombok.Data;

/**
 * @author ador
 *
 */
@Data
public class ApiCarBooking {
	
	private Long id;
	
	private Long carId;
	
	private String customerName;

	private String customerEmail;

	private String customerLincenceNumber;

	private Date bookStartDate;

	private Date bookEndDate;

	private Usage usage;

	private Set<String> foreignCountries;

	public static enum Usage {
		DOMESTIC, FOREIGN
	}
}
