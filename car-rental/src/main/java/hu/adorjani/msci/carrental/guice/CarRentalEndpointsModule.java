package hu.adorjani.msci.carrental.guice;

import com.google.api.server.spi.guice.EndpointsModule;
import com.google.common.collect.ImmutableList;
import com.googlecode.objectify.ObjectifyFilter;

import hu.adorjani.msci.carrental.api.CarRentalCloudEndpoint;

public class CarRentalEndpointsModule extends EndpointsModule {
	@Override
	protected void configureServlets() {
		bind(CarRentalCloudEndpoint.class).toInstance(new CarRentalCloudEndpoint());
	    configureEndpoints("/_ah/api/*", ImmutableList.of(CarRentalCloudEndpoint.class));
	    filter("/*").through(ObjectifyFilter.class);
	}
}
