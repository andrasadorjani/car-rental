/**
 * 
 */
package hu.adorjani.msci.carrental.api;

import java.util.List;

import com.google.api.server.spi.ServiceException;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.inject.Inject;

/**
 * @author ador
 *
 */
@Api(
name = "carRental",
version = "v1",
description = "This servcie lets to manage car booking related operations")
public class CarRentalCloudEndpoint {
	
	@Inject
	private ApiCarRentalService carRentalService;
	
	@ApiMethod(httpMethod = HttpMethod.GET, path="cars")
	public List<ApiCarListItem> listAvailableCars () throws ServiceException {
		try {
			return carRentalService.getAvailableCars();
		} catch (Exception e) {
			throw ApiErrorConverter.convert(e);
		}
	}
	
	@ApiMethod(httpMethod = HttpMethod.POST, path="car")
	public ApiCar saveCar (ApiCar newCar) throws ServiceException {
		try {
			return carRentalService.saveCar(newCar);
		} catch (Exception e) {
			throw ApiErrorConverter.convert(e);
		}
	}
	
	@ApiMethod(httpMethod = HttpMethod.GET, path="car/{carId}")
	public ApiCar getCar (@Named("carId") Long carId) throws ServiceException {
		try {
			return carRentalService.getCarById(carId);
		} catch (Exception e) {
			throw ApiErrorConverter.convert(e);
		}
	}
	
	@ApiMethod(httpMethod = HttpMethod.POST, path="booking")
	public ApiCarBooking bookCar (
			ApiCarBooking apiCarBooking) throws ServiceException {
		try {
			return carRentalService.bookACar(apiCarBooking);
		} catch (Exception e) {
			throw ApiErrorConverter.convert(e);
		}
	}

}
