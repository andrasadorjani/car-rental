/**
 * 
 */
package hu.adorjani.msci.carrental.domain;

import com.google.common.base.Preconditions;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Parent;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * @author ador
 *
 */
@NoArgsConstructor
public class AbstractEntityWithParent<P extends AbstractEntity> extends AbstractEntity {
	
	private static final long serialVersionUID = 1;
	
	@Getter(value = AccessLevel.PROTECTED)
	@NonNull
	@Load
	@Parent
	private Key<P> parentKey;
	
	protected AbstractEntityWithParent(Key<P> parentKey) {
		this.parentKey = Preconditions.checkNotNull(parentKey);
	}
	
	protected String getParentId() {
		return parentKey.getName();
	}
}
