package hu.adorjani.msci.carrental.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyFilter;

import hu.adorjani.msci.carrental.api.ApiCarRentalService;
import hu.adorjani.msci.carrental.api.ApiCarRentalServiceImpl;
import hu.adorjani.msci.carrental.business.CarRentalService;
import hu.adorjani.msci.carrental.business.CarRentalServiceImpl;

public class CarRentalBusinessModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(ObjectifyFilter.class).in(Singleton.class);
		bind(ApiCarRentalService.class).to(ApiCarRentalServiceImpl.class);
		bind(CarRentalService.class).to(CarRentalServiceImpl.class);
	}

}
